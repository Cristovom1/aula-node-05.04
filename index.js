const express = require('express');
const app = express();
const port = 8000;

//habilitar servidor
app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());


//--------------- Cliente ---------------

//get
app.get('/cliente',(req, res) => {
    console.log("Acessando:");
    let valores = req.query;
    res.send(valores.nome + " " + valores.sobrenome);
    console.log(valores.nome, valores.sobrenome);
});

//Post
app.use(bodyParser.json());
app.post('/cliente/post',(req, res) => {
    let access = req.headers["access"];
    if(access == 'EssaSenha'){
        let postClient = req.body;
        console.log("Nome: " + postClient.nome);
        console.log("Sobrenome: " + postClient.sobrenome);
        res.send("Success");
    } else{
        res.send("inválido");
    }
});

//Delete
app.use(bodyParser.json());
app.delete('/cliente/delete/:id',(req, res) => {
    let delClient = req.params;
    if(delClient == 'EssaSenha'){
        res.send("Success");
    } else{
        res.send("inválido");
    }
});

//Put
app.use(bodyParser.json());
app.put('/cliente/put',(req, res) => {
    let access = req.headers["access"];
    if(access == 'EssaSenha'){
        let putClient = req.body;
        console.log("Nome: " + putClient.nome);
        console.log("Sobrenome: " + putClient.sobrenome);
        res.send("Success");
    } else{
        res.send("inválido");
    }
});


//--------------- Funcionario ---------------

//get
app.get('/funcionario',(req, res) => {
    console.log("Acessando o recurso FUNCIONARIO");
    let getFun = req.query;
    res.send(getFun.nome + " " + getFun.sobrenome);
    console.log(getFun.nome, getFun.sobrenome);
});

//Post
app.use(bodyParser.json());
app.post('/funcionario/post',(req, res) => {
    let access = req.headers["access"];
    if(access == 'EssaSenha'){
        let postFun = req.body;
        console.log("Nome: " + postFun.nome);
        console.log("Sobrenome: " + postFun.sobrenome);
        res.send("Success");
    } else{
        res.send("inválido");
    }
});

//Delete
app.use(bodyParser.json());
app.delete('/funcionario/delete/:1010',(req, res) => {
    let delFun = req.params;
    if(delFun == 'EssaSenha'){
        res.send("Success");
    } else{
        res.send("inválido");
    }
});

//Put
app.use(bodyParser.json());
app.put('/funcionario/put',(req, res) => {
    let access = req.headers["access"];
    if(access == 'EssaSenha'){
        let putFun = req.body;
        console.log("Nome: " + putFun.nome);
        console.log("Sobrenome: " + putFun.sobrenome);
        res.send("Success");
    } else{
        res.send("inválido");
    }
});
